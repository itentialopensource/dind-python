<!---
BUG ISSUE TEMPLATE:
Issues with the "bug" label track problems or errors.

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "bug" label:
https://gitlab.com/itentialopensource/dind-python/issues

Verify the issue you're about to submit isn't a duplicate.
--->

## Description

<!-- Write a short description of the proposed change. Add pictures and log output if possible. -->

### Steps to Reproduce

<!-- How can the bug be reproduced? THIS IS VERY IMPORTANT!  -->

### What is the current *bug* behavior?

<!-- What actually happens currently? -->

### What is the expected *correct* behavior?

<!-- What do you expect to happen? -->

### Affected Version or Tag

<!-- What version/tag of the project is affected? -->

### Possible Fixes

<!-- If possible, link to the line of code that might be responsible for the problem. -->

### Related Issues & Merge Requests

<!-- Mention any issue(s) or merge request(s) that this Merge Request closes or is related to. -->

/label ~bug
