#!/bin/bash

PROJECT_NAME="dind-python"
REGISTRY_URL="itentialopensource"

while IFS=, read -r PYTHON_VERSION GPG_KEY PYTHON_PIP_VERSION POETRY_VERSION; do
  DIRPATH=$(echo "$PYTHON_VERSION"|awk -F'.' '{print $1"."$2}')
  mkdir -p builds/"$DIRPATH"
  cp Dockerfile.template builds/"$DIRPATH"/Dockerfile
  sed -i '' \
    -e "s %%GPG_KEY%% $GPG_KEY g" \
    -e "s %%PYTHON_VERSION%% $PYTHON_VERSION g" \
    -e "s %%PYTHON_PIP_VERSION%% $PYTHON_PIP_VERSION g" \
    -e "s %%POETRY_VERSION%% $POETRY_VERSION g" \
    "builds/$DIRPATH/Dockerfile"
done < python_versions.csv

# Build & publish all images
LATEST="0"
LATEST_PATH=""

# Version compare tool for finding latest versions
latest_version () {
  if (( $(echo "$1 > $2" | bc -l) )); then
    return 1
  else
    return 0
  fi
}

if [ "$1" == "--build-all" ] || [ "$1" == "-b" ]; then
  # Get the directories of each Dockerfile
  while read -r DIR; do
    # Create an array of all of the version paths
    VERSIONS=("$(dirname "$DIR")")
    for VERSION_PATH in "${VERSIONS[@]}"; do
      # get the version tag
      VERSION_TAG="$(basename "$VERSION_PATH")"
      latest_version "$VERSION_TAG" "$LATEST"
      if [ $? -eq 1 ]; then
        LATEST="$VERSION_TAG"
        LATEST_PATH="$VERSION_PATH"
      fi
      # move into the version's directory to build & publish the docker image.
      cd "$VERSION_PATH" || exit 1
      docker build -t "$REGISTRY_URL"/"$PROJECT_NAME":"$VERSION_TAG" . || exit 1
      if [ "$2" == "--push-all" ] || [ "$2" == "-p" ]; then
        docker push "$REGISTRY_URL"/"$PROJECT_NAME":"$VERSION_TAG" || exit 1
      fi
      cd ~- || exit 1
      echo -e "\\n\\n\\nDocker image for $VERSION_TAG has been built!\\n\\n\\n"
    done
  done < <(find . -name Dockerfile)

  if [ "$2" == "--push-all" ] || [ "$2" == "-p" ]; then
    # Create  latest tag with the newest version
    echo "Creating 'latest' tag from the following directory: $LATEST_PATH"
    cd "$LATEST_PATH" || exit 1
    docker build -t "$REGISTRY_URL"/"$PROJECT_NAME":latest . || exit 1
    docker push "$REGISTRY_URL"/"$PROJECT_NAME":latest || exit 1
    cd ~- || exit 1
  fi
fi
